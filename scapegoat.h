#ifndef SCAPEGOAT_H
#define SCAPEGOAT_H

#include "array.h"

typedef struct scapegoat_node {
  void *parent;
  void *key;
  void *value;
  void *left;
  void *right;
} ScapegoatNode;

ScapegoatNode *scapegoat_node_create(void *key, void *value);
ULONG scapegoat_node_size(ScapegoatNode *this);
void scapegoat_node_destroy(ScapegoatNode **this);
void scapegoat_node_linked_destroy(ScapegoatNode **this);

typedef struct scapegoat_tree {
  void *root;
  ULONG n;
  // number of elements in tree
  ULONG q;
  // overestimate of n
} ScapegoatTree;

ScapegoatTree *scapegoat_tree_create(ScapegoatNode *root);
DLONG scapegoat_tree_insert_with_depth(ScapegoatTree *this, ScapegoatNode *node, DLONG (*compare)(void *other, void *key));
void scapegoat_tree_insert(ScapegoatTree *this, ScapegoatNode *node, DLONG (*compare)(void*, void*));
ScapegoatNode *scapegoat_tree_find(ScapegoatTree *this, void *key, DLONG (*compare)(void*, void*));
void scapegoat_tree_rebuild(ScapegoatTree *this, ScapegoatNode *root);
int scapegoat_tree_pack(ScapegoatNode **this, ScapegoatNode *parent, int i);
ScapegoatNode *scapegoat_tree_build(ScapegoatNode **this, int i, int size);
void scapegoat_tree_destroy(ScapegoatTree **this);

double log32(double q);
#endif

// scapegt tree
// check if height over
// rebuild
// todo balanced tree struct without much rebuild
