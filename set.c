#include "set.h"
SetNode *set_node_create(void *key, void *value) {
  SetNode *this = (SetNode*)calloc(1, sizeof(SetNode));
  this->key = key;
  this->value = value;
  return this;
}
void set_node_destroy(SetNode **this) {
  if(*this == NULL) {
    return;
  }
  free(*this);
  *this = NULL;
}
Set *set_create(int min, int max) {
  Set *this = (Set*)calloc(1, sizeof(Set));
  this->min = min;
  this->max = max;
  this->inserts = 0;
  this->entries = (SetNode**)calloc(this->max, sizeof(SetNode*));
  return this;
}
void set_insert(Set *this, SetNode *node) {
  int *position = (int*)node->key;
  if(this->entries[*position] != NULL) {
    return;
  }
  this->entries[*position] = node;
  this->inserts++;
}
int set_exists(Set *this, void *key) {
  int *position = (int*)key;
  // todo use callback here instead perhaps
  if((this->min <= *position) && (*position <= this->max) && (this->entries[*position] != NULL)) {
    return TRUE;
  }
  return FALSE;
}
int set_empty(Set *this) {
  return this->inserts;
}
void set_intersect(Set *this, Set *one, Set *two) {
  // assume this, one, two are of same size, same min, max
  // assume if position is occupied it is same element
  int i = this->min, max = this->max;
  for(; i < max; i++) {
    if((one->entries[i] != NULL) && (two->entries[i] != NULL)) {
      set_insert(this, one->entries[i]);
    }
  }
}
void set_union(Set *this, Set *one, Set *two) {
  // assume this, one, two are of same size, same min, max
  // assume if position is occupied it is same element
  int i = this->min, max = this->max;
  for(; i < max; i++) {
    if(one->entries[i] != NULL) {
      set_insert(this, one->entries[i]);
      continue;
    }
    if(two->entries[i] != NULL) {
      set_insert(this, two->entries[i]);
    }
  }
}
void set_clear(Set *this) {
  int i = 0, max = this->max;
  for(; i < max; i++) {
    set_node_destroy(&(this->entries[i]));
  }
}
void set_destroy(Set **this) {
  free((*this)->entries);
  free(*this);
  *this = NULL;
}
