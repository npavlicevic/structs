#include "single_list_test.h"

void single_node_create_test() {
  // single node create
  SingleNode *node = single_node_create(calloc(1, sizeof(int)), calloc(1, sizeof(int)));
  assert(node);
  single_node_destroy(&node);
}
void single_node_destroy_test() {
  // single node destroy
  SingleNode *node = single_node_create(calloc(1, sizeof(int)), calloc(1, sizeof(int)));
  single_node_destroy(&node);
  assert(!node);
}
void list_create_test() {
  // create list test
  List *list = list_create(NULL);
  assert(list);
  list_destroy(&list);
}
void list_insert_test() {
  int *key, *value;
  DLONG capacity = 1024 * 1024, i = 0;
  time_t t;
  srand((unsigned)time(&t));
  key = (int*)calloc(1, sizeof(int));
  *key = rand() % capacity;
  value = (int*)calloc(1, sizeof(int));
  *value = rand() % capacity;
  SingleNode *root = single_node_create(key, value), *node;
  List *list = list_create(root);
  for(; i < capacity; i++) {
    key = (int*)calloc(1, sizeof(int));
    *key = rand() % capacity;
    value = (int*)calloc(1, sizeof(int));
    *value = rand() % capacity;
    node = single_node_create(key, value);
    list_insert(list, node);
  }
  assert(list_find(list, key, int_compare));
  list_destroy(&list);
}
void list_destroy_test() {
  // destroy list test
  List *list = list_create(NULL);
  list_destroy(&list);
  assert(!list);
}
