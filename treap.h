#ifndef TREAP_H
#define TREAP_H
#include <limits.h>
#include <time.h>
#include "array.h"
typedef struct treap_node {
  void *parent;
  void *key;
  void *value;
  void *left;
  void *right;
  int priority;
} TreapNode;
TreapNode *treap_node_create(void *key, void *value);
void treap_node_rotate_left_ch(TreapNode **this, DLONG (*compare)(void *other, void *key));
void treap_node_rotate_right_ch(TreapNode **this, DLONG (*compare)(void *other, void *key));
void treap_node_destroy(TreapNode **this);
void treap_node_linked_destroy(TreapNode **this);
typedef struct treap {
  void *root;
} Treap;
Treap *treap_create(TreapNode *root);
DLONG treap_insert(Treap *this, TreapNode *node, DLONG (*compare)(void *other, void *key));
TreapNode *treap_find(Treap *this, void *key, DLONG (*compare)(void *other, void *key));
void treap_bubble(Treap *this, TreapNode *node, DLONG (*compare)(void *other, void *key));
void treap_destroy(Treap **this);
void seed();
// TODO remove
#endif
