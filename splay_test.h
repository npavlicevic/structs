#ifndef SPLAY_TEST_H
#define SPLAY_TEST_H
#include <assert.h>
#include <time.h>
#include "splay.h"
void splay_node_create_test();
void splay_node_destroy_test();
void splay_tree_create_test();
void splay_tree_insert_test();
void splay_tree_destroy_test();
#endif
