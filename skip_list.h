#ifndef SKIP_LIST_H
#define SKIP_LIST_H
#include <time.h>
#include "array.h"
typedef struct skip_node {
  void *key;
  void *value;
  struct skip_node **next_in_line;
  // todo use void here
} SkipNode;
SkipNode *skip_node_create(void *key, void *value, int max_height);
SkipNode *skip_node_find(SkipNode *this, int height, void *key, DLONG (*compare)(void *other, void *key));
void skip_node_destroy(SkipNode **this);
void skip_node_linked_destroy(SkipNode **this);
typedef struct skip_list {
  SkipNode *root;
  int max_height;
  int height;
} SkipList;
SkipList *skip_list_create(SkipNode *node, int max_height);
void skip_list_insert(SkipList *this, SkipNode *node, DLONG (*compare)(void *other, void *key));
int skip_list_height(SkipList *this);
SkipNode *skip_list_find(SkipList *this, void *key, DLONG (*compare)(void *other, void *key));
void skip_list_destroy(SkipList **this);
void seed();
#endif
