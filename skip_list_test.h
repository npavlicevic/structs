#ifndef SKIP_LIST_TEST_H
#define SKIP_LIST_TEST_H
#include "skip_list.h"
#include <assert.h>
void skip_list_create_test();
void skip_list_insert_test();
void skip_list_destroy_test();
#endif
