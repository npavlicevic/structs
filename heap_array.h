#ifndef HEAP_ARRAY_C
#define HEAP_ARRAY_C
#include "array.h"
typedef struct heap_array {
  int position;
  int capacity;
  void **items;
} HeapArray;

HeapArray *heap_array_create(int capacity);
void heap_array_push_and_up(HeapArray *this, void *item, DLONG direction, DLONG (*compare)(void *other, void *key));
void *heap_array_pop_and_down(HeapArray *this, int direction, DLONG (*compare)(void *other, void *key));
void heap_array_destroy(HeapArray **this);
// TODO try some of these structs as adj list
#endif
