#include "skip_list_test.h"
void skip_list_create_test() {
  int max_height = 32;
  SkipNode *root = skip_node_create(calloc(1, sizeof(int)), calloc(1, sizeof(int)), max_height);
  SkipList *list = skip_list_create(root, max_height);
  assert(list);
  skip_list_destroy(&list);
}
void skip_list_insert_test() {
  int max_height = 32;
  int moar_height = max_height * max_height * max_height * max_height;
  int i = 0;
  int *key = (int*)calloc(1, sizeof(int));
  *key = rand() % moar_height;
  int *value = (int*)calloc(1, sizeof(int));
  *value = rand() % moar_height;
  SkipNode *root = skip_node_create(key, value, max_height);
  SkipList *list = skip_list_create(root, max_height);
  assert(list);
  for(; i < moar_height; i++) {
    *key = rand() % moar_height;
    *value = rand() % moar_height;
    root = skip_node_create(key, value, max_height);
    skip_list_insert(list, root, int_compare);
  }
  assert(skip_list_find(list, key, int_compare));
  skip_list_destroy(&list);
}
void skip_list_destroy_test() {
  int max_height = 32;
  SkipNode *root = skip_node_create(calloc(1, sizeof(int)), calloc(1, sizeof(int)), max_height);
  SkipList *list = skip_list_create(root, max_height);
  skip_list_destroy(&list);
  assert(!list);
}
