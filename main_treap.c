#include "treap_test.h"

int main() {
  treap_seed_test();
  treap_node_create_test();
  treap_node_destroy_test();
  treap_create_test();
  treap_insert_test();
  treap_destroy_test();
  return 0;
}
