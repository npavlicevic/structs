#ifndef SCAPEGOAT_TEST_H
#define SCAPEGOAT_TEST_H

#include <assert.h>
#include <time.h>
#include "scapegoat.h"

void scapegoat_node_create_test();
void scapegoat_node_destroy_test();
void scapegoat_tree_create_test();
void scapegoat_tree_insert_test();
void scapegoat_tree_destroy_test();
#endif
