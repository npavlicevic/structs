#include "treap.h"

TreapNode *treap_node_create(void *key, void *value) {
  TreapNode *this = (TreapNode*)calloc(1, sizeof(TreapNode));
  this->parent = NULL;
  this->key = key;
  this->value = value;
  this->left = NULL;
  this->right = NULL;
  this->priority = rand() % INT_MAX;
  return this;
}
void treap_node_rotate_left_ch(TreapNode **this, DLONG (*compare)(void *other, void *key)) {
  TreapNode *child = (*this)->left, *parent = (*this)->parent;
  DLONG res = FALSE;
  (*this)->left = child->right;
  if((*this)->left) {
    ((TreapNode*)(*this)->left)->parent = *this;
  }
  child->right = *this;
  (*this)->parent = child;
  child->parent = parent;
  if(parent) {
    res = compare((*this)->key, parent->key);
    if(res < 0) {
      parent->left = child;
    } else {
      parent->right = child;
    }
  }
  *this = child;
}
void treap_node_rotate_right_ch(TreapNode **this, DLONG (*compare)(void *other, void *key)) {
  TreapNode *child = (*this)->right, *parent = (*this)->parent;
  DLONG res = FALSE;
  (*this)->right = child->left;
  if((*this)->right) {
    ((TreapNode*)(*this)->right)->parent = *this;
  }
  child->left = *this;
  (*this)->parent = child;
  child->parent = parent;
  if(parent) {
    res = compare((*this)->key, parent->key);
    if(res < 0) {
      parent->left = child;
    } else {
      parent->right = child;
    }
  }
  *this = child;
}
void treap_node_destroy(TreapNode **this) {
  free(*this);
  *this = NULL;
}
void treap_node_linked_destroy(TreapNode **this) {
  if(*this == NULL) {
    return;
  }
  TreapNode *root = *this, *previous, *next;
  while(root) {
    if(previous == root->parent) {
      if(root->left) {
        next = root->left;
      } else if(root->right) {
        next = root->right;
      } else {
        next = root->parent;
      }
    } else if(previous == root->left) {
      if(root->right) {
        next = root->right;
      } else {
        next = root->parent;
      }
    } else {
      next = root->parent;
    }
    previous = root;
    if(next == root->parent) {
      treap_node_destroy(&root);
    }
    root = next;
  }
}
Treap *treap_create(TreapNode *root) {
  Treap *this = (Treap*)calloc(1, sizeof(Treap));
  this->root = root;
  return this;
}
DLONG treap_insert(Treap *this, TreapNode *node, DLONG (*compare)(void *other, void *key)) {
  TreapNode *root = this->root;
  DLONG done = FALSE, res = FALSE;
  while(!done) {
    res = compare(node->key, root->key);
    if(res < 0) {
      if(!root->left) {
        root->left = node;
        node->parent = root;
        done = TRUE;
      } else {
        root = root->left;
      }
    } else if(res > 0) {
      if(!root->right) {
        root->right = node;
        node->parent = root;
        done = TRUE;
      } else {
        root = root->right;
      }
    } else {
      return done;
    }
  }
  treap_bubble(this, node, compare);
  return done;
}
TreapNode *treap_find(Treap *this, void *key, DLONG (*compare)(void *other, void *key)) {
  TreapNode *root = this->root;
  DLONG res = FALSE;
  while(root) {
    res = compare(key, root->key);
    if(res < 0) {
      root = root->left;
    } else if(res > 0) {
      root = root->right;
    } else {
      return root;
    }
  }
  return root;
}
void treap_bubble(Treap *this, TreapNode *node, DLONG (*compare)(void *other, void *key)) {
  TreapNode *current = node, *parent;
  DLONG res = FALSE, side = FALSE;
  for(;;) {
    if(!current->parent) {
      this->root = current;
      return;
    }
    parent = current->parent;
    res = compare(&(current->priority), &(parent->priority));
    if(res > 0) {
      return;
    }
    side = compare(current->key, parent->key);
    current = parent;
    if(side < 0) {
      treap_node_rotate_left_ch(&current, compare);
    } else if(side > 0) {
      treap_node_rotate_right_ch(&current, compare);
    }
  }
}
void treap_destroy(Treap **this) {
  TreapNode *root = (*this)->root;
  treap_node_linked_destroy(&root);
  free(*this);
  *this = NULL;
}
void seed() {
  time_t t;
  srand((unsigned)time(&t));
}
