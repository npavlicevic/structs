#include "treap_test.h"

void treap_seed_test() {
  seed();
}
void treap_node_create_test() {
  TreapNode *node = treap_node_create((int*)calloc(1, sizeof(int)), (int*)calloc(1, sizeof(int)));
  assert(node);
  treap_node_destroy(&node);
}
void treap_node_destroy_test() {
  TreapNode *node = treap_node_create((int*)calloc(1, sizeof(int)), (int*)calloc(1, sizeof(int)));
  treap_node_destroy(&node);
  assert(!node);
}
void treap_create_test() {
  Treap *tree = treap_create(treap_node_create((int*)calloc(1, sizeof(int)), (int*)calloc(1, sizeof(int))));
  assert(tree);
  treap_destroy(&tree);
}
void treap_insert_test() {
  int *key, *value;
  DLONG capacity = 1024 * 1024, i = 0;
  time_t t;
  srand((unsigned)time(&t));
  key = (int*)calloc(1, sizeof(int));
  *key = rand() % capacity;
  value = (int*)calloc(1, sizeof(int));
  *value = rand() % capacity;
  TreapNode *root = treap_node_create(key, value), *node;
  Treap*tree = treap_create(root);
  for(; i < capacity; i++) {
    key = (int*)calloc(1, sizeof(int));
    *key = rand() % capacity;
    value = (int*)calloc(1, sizeof(int));
    *value = rand() % capacity;
    node = treap_node_create(key, value);
    treap_insert(tree, node, int_compare);
  }
  assert(treap_find(tree, key, int_compare));
  treap_destroy(&tree);
}
void treap_destroy_test() {
  Treap *tree = treap_create(treap_node_create((int*)calloc(1, sizeof(int)), (int*)calloc(1, sizeof(int))));
  treap_destroy(&tree);
  assert(!tree);
}
