#include "skip_list.h"
SkipNode *skip_node_create(void *key, void *value, int max_height) {
  SkipNode *this = (SkipNode*)calloc(1, sizeof(SkipNode));
  this->key = key;
  this->value = value;
  this->next_in_line = (SkipNode**)calloc(max_height, sizeof(SkipNode*));
  return this;
}
SkipNode *skip_node_find(SkipNode *this, int height, void *key, DLONG (*compare)(void *other, void *key)) {
  int i = height;
  SkipNode *next;
  for(; i >= 0; i--) {
    while(this) {
      if(!compare(key, this->key)) {
        return this;
      }
      next = this->next_in_line[i];
      if(!next) {
        break;
      }
      if(compare(key, next->key) > 0) {
        break;
      }
      this = next;
    }
  }
  return NULL;
}
void skip_node_destroy(SkipNode **this) {
  free(*this);
  *this = NULL;
}
void skip_node_linked_destroy(SkipNode **this) {
  SkipNode *root = *this, *prev;
  while(root) {
    prev = root;
    root = root->next_in_line[0];
    skip_node_destroy(&prev);
  }
  *this = NULL;
}
SkipList *skip_list_create(SkipNode *node, int max_height) {
  SkipList *this = (SkipList*)calloc(1, sizeof(SkipList));
  this->root = node;
  this->max_height = max_height;
  this->height = 0;
  return this;
}
void skip_list_insert(SkipList *this, SkipNode *node, DLONG (*compare)(void *other, void *key)) {
  // todo move this to node
  int height = skip_list_height(this);
  int i = this->height;
  SkipNode *root = this->root, *next;
  for(; i >= 0; i--) {
    while(root->next_in_line[i] && (root = root->next_in_line[i])) {
      next = root->next_in_line[i];
      if(!next) {
        break;
      }
      if(compare(next->key, node->key) > 0) {
        break;
      }
      root = next;
    }
    if(i > height) {
      continue;
    }
    node->next_in_line[i] = root->next_in_line[i];
    root->next_in_line[i] = node;
  }
}
int skip_list_height(SkipList *this) {
  int height = 0;
  while((++height) < this->max_height) {
    if(!(rand() % 2)) break;
    if(height > this->height) {
      this->height = height;
      break;
    }
  }
  return height;
}
SkipNode *skip_list_find(SkipList *this, void *key, DLONG (*compare)(void *other, void *key)) {
  return skip_node_find(this->root, this->height, key, compare);
}
void skip_list_destroy(SkipList **this) {
  skip_node_linked_destroy(&((*this)->root));
  free(*this);
  *this = NULL;
}
void seed() {
  time_t t;
  srand((unsigned)time(&t));
}
