#include "splay_test.h"

int main() {
  splay_node_create_test();
  splay_node_destroy_test();
  splay_tree_create_test();
  splay_tree_insert_test();
  splay_tree_destroy_test();
  return 0;
}
