#include "set_test.h"

void set_intersect_test() {
  int min = 0, max = 3, pos[4] = {0, 1, 2, 3};
  SetNode *_one = set_node_create(&pos[0], &pos[0]);
  SetNode *_two = set_node_create(&pos[1], &pos[1]);
  SetNode *_three = set_node_create(&pos[2], &pos[2]);
  SetNode *_four = set_node_create(&pos[3], &pos[3]);
  Set *set = set_create(min, max);
  Set *one = set_create(min, max);
  set_insert(one, _one);
  set_insert(one, _two);
  set_insert(one, _three);
  Set *two = set_create(min, max);
  set_insert(two, _three);
  set_insert(two, _four);
  set_intersect(set, one, two);
  assert(!set_exists(set, _one->key));
  assert(set_exists(set, _three->key));
  set_node_destroy(&_one);
  set_node_destroy(&_two);
  set_node_destroy(&_three);
  set_node_destroy(&_four);
  set_destroy(&one);
  set_destroy(&two);
  set_destroy(&set);
}
void set_union_test() {
  int min = 0, max = 3, pos[4] = {0, 1, 2, 3};
  SetNode *_one = set_node_create(&pos[0], &pos[0]);
  SetNode *_two = set_node_create(&pos[1], &pos[1]);
  SetNode *_three = set_node_create(&pos[2], &pos[2]);
  SetNode *_four = set_node_create(&pos[3], &pos[3]);
  Set *set = set_create(min, max);
  Set *one = set_create(min, max);
  set_insert(one, _one);
  set_insert(one, _two);
  set_insert(one, _three);
  Set *two = set_create(min, max);
  set_insert(two, _three);
  set_insert(two, _four);
  set_union(set, one, two);
  assert(set_exists(set, _three->key));
  assert(set_exists(set, _four->key));
  set_clear(set);
  set_destroy(&set);
  set_destroy(&one);
  set_destroy(&two);
}
