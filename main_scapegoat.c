#include "scapegoat_test.h"

int main() {
  scapegoat_node_create_test();
  scapegoat_node_destroy_test();
  scapegoat_tree_create_test();
  scapegoat_tree_insert_test();
  scapegoat_tree_destroy_test();
  return 0;
}
