#ifndef SINGLE_LIST_H
#define SINGLE_LIST_H
#include "array.h"
typedef struct single_node {
  void *key;
  void *value;
  struct single_node *next;
} SingleNode;

SingleNode *single_node_create(void *key, void *value);
void single_node_destroy(SingleNode **this);
void single_node_linked_destroy(SingleNode **this);

typedef struct list {
  void *head;
  void *tail;
} List;

List *list_create(SingleNode *node);
void list_insert(List *this, SingleNode *node);
SingleNode *list_get(List *this);
void list_pop(List *this);
DLONG list_find(List *this, void *key, DLONG (*compare)(void *other, void *key));
void list_destroy(List **this);
#endif
