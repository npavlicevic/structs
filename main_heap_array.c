#include "heap_array_test.h"

int main() {
  heap_array_create_test();
  heap_array_sort_test();
  heap_array_sort_reverse_test();
  return 0;
}
