#include "single_list.h"

SingleNode *single_node_create(void *key, void *value) {
  // create single node
  SingleNode *this = (SingleNode*)calloc(1, sizeof(SingleNode));
  if(this == NULL) {
    return this;
  }
  this->key = key;
  this->value = value;
  this->next = NULL;
  return this;
}
void single_node_destroy(SingleNode **this) {
  // single node destroy
  if(*this == NULL) {
    return;
  }
  free(*this);
  *this = NULL;
}
void single_node_linked_destroy(SingleNode **this) {
  SingleNode *node;
  while(*this) {
    node = *this;
    *this = (*this)->next;
    free(node);
  }
  *this = NULL;
}
List *list_create(SingleNode *node) {
  // list create
  List *this = (List*)calloc(1, sizeof(List));
  if(this == NULL) {
    return this;
  }
  this->head = node;
  this->tail = NULL;
  return this;
}
void list_insert(List *this, SingleNode *node) {
  if(!this->head) {
    this->head = node;
    return;
  }
  if(!this->tail) {
    this->tail = node;
    ((SingleNode*)this->head)->next = this->tail;
    return;
  }
  ((SingleNode*)this->tail)->next = node;
  this->tail = node;
}
SingleNode *list_get(List *this) {
  return this->head;
}
void list_pop(List *this) {
  if(!this->head) {
    return;
  }
  SingleNode *node = this->head;
  this->head = ((SingleNode*)this->head)->next;
  if(!this->head) {
    this->head = this->tail = NULL;
  }
  free(node);
}
DLONG list_find(List *this, void *key, DLONG (*compare)(void *other, void *key)) {
  if(!this->head) {
    return FALSE;
  }
  SingleNode *node = this->head;
  while(node) {
    if(!compare(key, node->key)) {
      return TRUE;
    }
    node = node->next;
  }
  return FALSE;
}
void list_destroy(List **this) {
  // list destroy
  if(*this == NULL) {
    return;
  }
  SingleNode *node = (SingleNode*)(*this)->head;
  single_node_linked_destroy(&node);
  free(*this);
  *this = NULL;
}
