#include "heap_array_test.h"

void heap_array_create_test() {
  int capacity = 2048;
  int max = capacity / 2;
  int up = capacity / 4;
  int i = 0;
  int *item;
  int *one;
  int *two;
  int *three;
  HeapArray *priority = heap_array_create(capacity);
  for(; i < up; i++) {
    item = (int*)calloc(1, sizeof(int));
    *item = rand() % max;
    heap_array_push_and_up(priority, item, -1, int_compare);
  }
  one = heap_array_pop_and_down(priority, -1, int_compare);
  two = heap_array_pop_and_down(priority, -1, int_compare);
  three = heap_array_pop_and_down(priority, -1, int_compare);
  assert(*one < *two);
  assert(*two < *three);
  free(one);
  free(two);
  free(three);
  heap_array_destroy(&priority);
}
void heap_array_sort_test() {
  int capacity = 2048;
  int max = capacity / 2;
  int up = capacity / 4;
  int i = 0;
  int *item;
  int *one;
  int *two;
  void **items = (void**)calloc(up, sizeof(void*));
  HeapArray *priority = heap_array_create(capacity);
  for(; i < up; i++) {
    item = (int*)calloc(1, sizeof(int));
    *item = rand() % max;
    heap_array_push_and_up(priority, item, -1, int_compare);
  }
  for(i = 0; priority->position; i++) {
    item = (int*)heap_array_pop_and_down(priority, -1, int_compare);
    items[i] = item;
  }
  for(i = 1; i < up; i++) {
    one = (int*)items[i-1];
    if(one == NULL) continue;
    two = (int*)items[i];
    if(two == NULL) continue;
    assert(*one <= *two);
  }
  array_destroy(&items, up);
  heap_array_destroy(&priority);
}
void heap_array_sort_reverse_test() {
  int capacity = 2048;
  int max = capacity / 2;
  int up = capacity / 4;
  int i = 0;
  int *item;
  int *one;
  int *two;
  void **items = (void**)calloc(up, sizeof(void*));
  HeapArray *priority = heap_array_create(capacity);
  for(; i < up; i++) {
    item = (int*)calloc(1, sizeof(int));
    *item = rand() % max;
    heap_array_push_and_up(priority, item, 1, int_compare);
  }
  for(i = 0; priority->position; i++) {
    item = (int*)heap_array_pop_and_down(priority, 1, int_compare);
    items[i] = item;
  }
  for(i = 1; i < up; i++) {
    one = (int*)items[i-1];
    if(one == NULL) continue;
    two = (int*)items[i];
    if(two == NULL) continue;
    assert(*one >= *two);
  }
  array_destroy(&items, up);
  heap_array_destroy(&priority);
}
