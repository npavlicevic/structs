#ifndef TREAP_TEST_H
#define TREAP_TEST_H
#include <assert.h>
#include "treap.h"

void treap_seed_test();
void treap_node_create_test();
void treap_node_destroy_test();
void treap_create_test();
void treap_insert_test();
void treap_destroy_test();
#endif
