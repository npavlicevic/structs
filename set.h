#ifndef SET_H
#define SET_H
#include "array.h"
typedef struct set_node {
  void *key;
  void *value;
} SetNode;
SetNode *set_node_create(void *key, void *value);
void set_node_destroy(SetNode **this);
typedef struct set {
  int min;
  int max;
  int inserts;
  SetNode **entries;
} Set;
Set *set_create(int min, int max);
void set_insert(Set *this, SetNode *node);
int set_exists(Set *this, void *key);
int set_empty(Set *this);
void set_intersect(Set *this, Set *one, Set *two);
void set_union(Set *this, Set *one, Set *two);
void set_clear(Set *this);
void set_destroy(Set **this);
// todo this with hash
#endif
