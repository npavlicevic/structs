#include "skip_list_test.h"

int main() {
  seed();
  skip_list_create_test();
  skip_list_insert_test();
  skip_list_destroy_test();
  return 0;
}
