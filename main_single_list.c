#include "single_list_test.h"

int main() {
  single_node_create_test();
  single_node_destroy_test();
  list_create_test();
  list_insert_test();
  list_destroy_test();
  return 0;
}
