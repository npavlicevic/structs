#ifndef SPLAY_H
#define SPLAY_H
#include "array.h"

typedef struct splay_node {
  void *parent;
  void *key;
  void *value;
  void *left;
  void *right;
} SplayNode;
SplayNode *splay_node_create(void *key, void *value);
void splay_node_rotate_left_ch(SplayNode **this, DLONG (*compare)(void *other, void *key));
void splay_node_rotate_right_ch(SplayNode **this, DLONG (*compare)(void *other, void *key));
void splay_node_destroy(SplayNode **this);
void splay_node_linked_destroy(SplayNode **this);
typedef struct splay_tree {
  void *root;
} SplayTree;
SplayTree *splay_tree_create(SplayNode *root);
DLONG splay_tree_insert(SplayTree *this, SplayNode *node, DLONG (*compare)(void *other, void *key));
SplayNode *splay_tree_find(SplayTree *this, void *key, DLONG (*compare)(void *other, void *key));
void splay_tree_splay(SplayTree *this, SplayNode *node, DLONG (*compare)(void *other, void *key));
void splay_tree_destroy(SplayTree **this);
#endif
