#include "heap_array.h"

HeapArray *heap_array_create(int capacity) {
  HeapArray *this = (HeapArray*)calloc(1, sizeof(HeapArray));
  this->position = 1;
  this->capacity = capacity;
  this->items = (void**)calloc(this->capacity, sizeof(void*));
  return this;
}
void heap_array_push_and_up(HeapArray *this, void *item, DLONG direction, DLONG (*compare)(void *other, void *key)) {
  // push new element last and push up to position
  // left child 2i
  // right child 2i+1
  // parent i/2
  // comparison in terms of child
  // O(log n) worst time
  this->items[this->position] = item;
  int child = this->position;
  int parent = child / 2;
  for(;parent > 0;) {
    if(compare(this->items[child], this->items[parent]) == direction) {
      array_swap(this->items, child, parent);
      child = parent;
      parent /= 2;
    } else {
      break;
    }
  }
  this->position++;
}
void *heap_array_pop_and_down(HeapArray *this, int direction, DLONG (*compare)(void *other, void *key)) {
  // take last element and return first
  // comparison in terms of child
  // O(log n) worst case
  void *r = this->items[1];
  void *up = this->items[--this->position];
  int parent = 1;
  int left_child;
  int right_child;
  int direction_;
  // 0 left 1 right
  this->items[1] = up;
  this->items[this->position] = NULL;
  for(;1;) {
    left_child = 2 * parent;
    right_child = 2 * parent + 1;
    if(left_child >= this->position && right_child >= this->position) {
      break;
    }
    if(this->items[right_child] == NULL) {
      direction_ = 0;
    } else if(this->items[left_child] == NULL) {
      direction_ = 1;
    } else if(compare(this->items[left_child], this->items[right_child]) == direction) {
      direction_ = 0;
    } else {
      direction_ = 1;
    }
    if(!direction_) {
      if(compare(this->items[left_child], this->items[parent]) == direction) {
        array_swap(this->items, parent, left_child);
        parent = left_child;
      } else {
        break;
      }
    } else {
      if(compare(this->items[right_child], this->items[parent]) == direction) {
        array_swap(this->items, parent, right_child);
        parent = right_child;
      } else {
        break;
      }
    }
  }
  return r;
}
void heap_array_destroy(HeapArray **this) {
  int i, capacity;
  for(i = 0, capacity = (*this)->capacity; i < capacity; i++) {
    if((*this)->items[i] == NULL) {
      continue;
    }
    free((*this)->items[i]);
  }
  free((*this)->items);
  free(*this);
  *this = NULL;
}

