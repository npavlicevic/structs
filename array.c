#include "array.h"

DLONG int_compare(void *other, void *key) {
  int *other_ = (int*)other;
  int *key_ = (int*)key;
  if(*other_ < *key_) {
    return -1;
  } else if(*other_ > *key_) {
    return 1;
  }
  return 0;
}
void int_set(void **this, int position) {
  int max = 1024;
  // TODO make ~ max ~ param
  int *item = calloc(1, sizeof(int));
  *item = rand() % max;
  this[position] = item;
}
void int_print(void *this) {
  printf("%d ", *((int*)this));
}
void array_init(void **this, int n, void (*set)(void **this, int position)) {
  int i = 0;
  for(; i < n; i++) {
    set(this, i);
  }
}
void array_swap(void **this, int from, int to) {
  void *temp = this[to];
  this[to] = this[from];
  this[from] = temp;
}
void array_print(void **this, int n, void (*print_)(void *this)) {
  int i = 0;
  for(; i < n; i++) {
    print_(this[i]);
  }
}
void array_destroy(void ***this, int n) {
  int i = 0;
  for(; i < n; i++) {
    if((*this)[i] == NULL) {
      continue;
    }
    free((*this)[i]);
  }
  free(*this);
  *this = NULL;
}
void **array_create(ULONG size) {
  // array of pointers
  return (void**)calloc(size, sizeof(void*));
}
