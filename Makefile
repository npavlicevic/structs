#
# Makefile 
# structs
#

CC=gcc
AR=ar
CFLAGS=-g -std=c99 -Werror -Wall -Wextra -Wformat -Wformat-security -pedantic
OBJ_CFLAGS=-c -static
AR_FLAGS=rcs
LIBS=-lm 
LIBS_ARRAY=-larrayc
LIBS_HEAP_ARRAY=-lheaparrayc

OBJ_ARRAY=array.c
LIB_ARRAY=array.o
FILES=scapegoat.c scapegoat_test.c main_scapegoat.c
FILES_SPLAY=splay.c splay_test.c main_splay.c
FILES_TREAP=treap.c treap_test.c main_treap.c
FILES_LIST=single_list.c single_list_test.c main_single_list.c
OBJ_LIST=single_list.c
LIB_LIST=single_list.o
FILES_SKIP_LIST=skip_list.c skip_list_test.c main_skip_list.c
FILES_SET=set.c set_test.c main_set.c
OBJ_HEAP_ARRAY=heap_array.c
LIB_HEAP_ARRAY=heap_array.o
FILES_HEAP_ARRAY=heap_array_test.c main_heap_array.c
CLEAN=array.o libarrayc.a main_scapegoat main_splay main_treap main_single_list single_list.o libsinglelist.a main_skip_list main_set heaparray

all: objarray libarray scapegoat splay treap singlelist objsinglelist libsinglelist skiplist set objheaparray libheaparray heaparray

objarray: ${OBJ_ARRAY}
	${CC} ${OBJ_CFLAGS} $^ -o array.o ${LIBS}

libarray: ${LIB_ARRAY}
	${AR} ${AR_FLAGS} libarrayc.a $^

scapegoat: ${FILES}
	${CC} ${CFLAGS} $^ -o main_scapegoat ${LIBS} ${LIBS_ARRAY}

splay: ${FILES_SPLAY}
	${CC} ${CFLAGS} $^ -o main_splay ${LIBS} ${LIBS_ARRAY}

treap: ${FILES_TREAP}
	${CC} ${CFLAGS} $^ -o main_treap ${LIBS} ${LIBS_ARRAY}

singlelist: ${FILES_LIST}
	${CC} ${CFLAGS} $^ -o main_single_list ${LIBS} ${LIBS_ARRAY}

objsinglelist: ${OBJ_LIST}
	${CC} ${OBJ_CFLAGS} $^ -o single_list.o ${LIBS} ${LIBS_ARRAY}

libsinglelist: ${LIB_LIST}
	${AR} ${AR_FLAGS} libsinglelist.a  $^

skiplist: ${FILES_SKIP_LIST}
	${CC} ${CFLAGS} $^ -o main_skip_list ${LIBS} ${LIBS_ARRAY}

set: ${FILES_SET}
	${CC} ${CFLAGS} $^ -o main_set ${LIBS} ${LIBS_ARRAY}

objheaparray: ${OBJ_HEAP_ARRAY}
	${CC} ${OBJ_CFLAGS} $^ -o heap_array.o ${LIBS} ${LIBS_ARRAY}

libheaparray: ${LIB_HEAP_ARRAY}
	${AR} ${AR_FLAGS} libheaparrayc.a $^

heaparray: ${FILES_HEAP_ARRAY}
	${CC} ${CFLAGS} $^ -o main_heap_array ${LIBS} ${LIBS_ARRAY} ${LIBS_HEAP_ARRAY}

clean: ${CLEAN}
	rm $^
