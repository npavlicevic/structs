#include "splay.h"
SplayNode *splay_node_create(void *key, void *value) {
  SplayNode *this = (SplayNode*)calloc(1, sizeof(SplayNode));
  this->parent = NULL;
  this->key = key;
  this->value = value;
  this->left = NULL;
  this->right = NULL;
  return this;
}
void splay_node_rotate_left_ch(SplayNode **this, DLONG (*compare)(void *other, void *key)) {
  SplayNode *child = (SplayNode*)(*this)->left, *gparent = (SplayNode*)(*this)->parent;
  DLONG res = FALSE;
  (*this)->left = child->right;
  if((*this)->left != NULL) {
    ((SplayNode*)(*this)->left)->parent = (*this);
  }
  child->right = *this;
  child->parent = gparent;
  if(gparent) {
    res = compare((*this)->key, gparent->key);
    if(res < 0) {
      gparent->left = child;
    } else if(res > 0) {
      gparent->right = child;
    }
  }
  (*this)->parent = child;
  *this = child;
}
void splay_node_rotate_right_ch(SplayNode **this, DLONG (*compare)(void *other, void *key)) {
  SplayNode *child = (SplayNode*)(*this)->right, *gparent = (*this)->parent;
  DLONG res = FALSE;
  (*this)->right = child->left;
  if((*this)->right != NULL) {
    ((SplayNode*)(*this)->right)->parent = (*this);
  }
  child->left = *this;
  child->parent = gparent;
  if(gparent) {
    res = compare((*this)->key, gparent->key);
    if(res < 0) {
      gparent->left = child;
    } else if(res > 0) {
      gparent->right = child;
    }
  }
  (*this)->parent = child;
  *this = child;
}
void splay_node_destroy(SplayNode **this) {
  free(*this);
  *this = NULL;
}
void splay_node_linked_destroy(SplayNode **this) {
  if(*this == NULL) {
    return;
  }
  SplayNode *root = (*this), *previous, *next;
  while(root) {
    if(previous == root->parent) {
      // todo use funcs for comparisons for generic
      if(root->left) {
        next = root->left;
      } else if(root->right) {
        next = root->right;
      } else {
        next = root->parent;
      }
    } else if(previous == root->left) {
      if(root->right) {
        next = root->right;
      } else {
        next = root->parent;
      }
    } else {
      next = root->parent;
    }
    previous = root;
    if(next == root->parent) {
      splay_node_destroy(&root);
      // todo use callback here to make generic
    }
    root = next;
  }
}
SplayTree *splay_tree_create(SplayNode *root) {
  SplayTree *this = (SplayTree*)calloc(1, sizeof(SplayTree));
  this->root = root;
  return this;
}
DLONG splay_tree_insert(SplayTree *this, SplayNode *node, DLONG (*compare)(void *other, void *key)) {
  // todo don't need other stuff
  SplayNode *root = this->root;
  DLONG  done = FALSE, res = FALSE;
  do {
    res = compare(node->key, root->key);
    if(res < 0) {
      if(root->left == NULL) {
        root->left = node;
        node->parent = root;
        done = TRUE;
      } else {
        root = root->left;
      }
    } else if(res > 0) {
      if(root->right == NULL) {
        root->right = node;
        node->parent = root;
        done = TRUE;
      } else {
        root = root->right;
      }
    } else {
      return done;
    }
  } while(!done);
  splay_tree_splay(this, node, compare);
  return done;
}
SplayNode *splay_tree_find(SplayTree *this, void *key, DLONG (*compare)(void *other, void *key)) {
  // todo make this generic for binary trees
  SplayNode *root = this->root;
  DLONG res = FALSE;
  while(root) {
    res = compare(key, root->key);
    if(!res) {
      return root;
    } else if(res < 0) {
      root = root->left;
    } else {
      root = root->right;
    }
  }
  return root;
}
void splay_tree_splay(SplayTree *this, SplayNode *node, DLONG (*compare)(void *other, void *key)) {
  SplayNode *current = node, *parent, *gparent;
  DLONG res = FALSE, gres = FALSE;
  for(;;) {
    parent = (SplayNode*)current->parent;
    if(parent == NULL) {
      break;
    }
    gparent = (SplayNode*)parent->parent;
    if(gparent == NULL) {
      res = compare(current->key, parent->key);
      current = parent;
      if(res < 0) {
        splay_node_rotate_left_ch(&current, compare);
      } else if(res > 0) {
        splay_node_rotate_right_ch(&current, compare);
      }
      break;
    }
    res = compare(current->key, parent->key);
    gres = compare(parent->key, gparent->key);
    current = gparent;
    if(res < 0 && gres < 0) {
      splay_node_rotate_left_ch(&current, compare);
      splay_node_rotate_left_ch(&current, compare);
      continue;
    }
    if(res > 0 && gres > 0) {
      splay_node_rotate_right_ch(&current, compare);
      splay_node_rotate_right_ch(&current, compare);
      continue;
    }
    if(res < 0 && gres > 0) {
      splay_node_rotate_right_ch(&current, compare);
      splay_node_rotate_left_ch(&current, compare);
      continue;
    }
    if(res > 0 && gres < 0) {
      splay_node_rotate_left_ch(&current, compare);
      splay_node_rotate_right_ch(&current, compare);
    }
  }
  this->root = current;
}
void splay_tree_destroy(SplayTree **this) {
  SplayNode *root = (SplayNode*)(*this)->root;
  splay_node_destroy(&root);
  // todo this for real
  free(*this);
  *this = NULL;
}
// todo add remove
