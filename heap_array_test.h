#ifndef HEAP_ARRAY_TEST_H
#define HEAP_ARRAY_TEST_H
#include <assert.h>
#include "heap_array.h"
void heap_array_create_test();
void heap_array_sort_test();
void heap_array_sort_reverse_test();
#endif
