#include "splay_test.h"

void splay_node_create_test() {
  // splay node create test
  SplayNode *node = splay_node_create((int*)calloc(1, sizeof(int)), (int*)calloc(1, sizeof(int)));
  assert(node);
  splay_node_destroy(&node);
}
void splay_node_destroy_test() {
  // splay node destroy test
  SplayNode *node = splay_node_create((int*)calloc(1, sizeof(int)), (int*)calloc(1, sizeof(int)));
  splay_node_destroy(&node);
  assert(!node);
}
void splay_tree_create_test() {
  // splay tree create test
  SplayNode *root = splay_node_create((int*)calloc(1, sizeof(int)), (int*)calloc(1, sizeof(int)));
  SplayTree *tree = splay_tree_create(root);
  assert(tree);
  splay_tree_destroy(&tree);
}
void splay_tree_insert_test() {
  // splay tree insert test
  int *key, *value;
  DLONG capacity = 1024 * 1024, i = 0, res;
  time_t t;
  srand((unsigned)time(&t));
  key = (int*)calloc(1, sizeof(int));
  *key = rand() % capacity;
  value = (int*)calloc(1, sizeof(int));
  *value = rand() % capacity;
  SplayNode *root = splay_node_create(key, value), *node;
  SplayTree *tree = splay_tree_create(root);
  for(; i < capacity; i++) {
    key = (int*)calloc(1, sizeof(int));
    *key = rand() % capacity;
    value = (int*)calloc(1, sizeof(int));
    *value = rand() % capacity;
    node = splay_node_create(key, value);
    res = splay_tree_insert(tree, node, int_compare);
    if(!res) {
      splay_node_destroy(&node);
    }
  }
  assert(splay_tree_find(tree, key, int_compare));
  splay_tree_destroy(&tree);
}
void splay_tree_destroy_test() {
  // splay tree destroy test
  SplayNode *root = splay_node_create((int*)calloc(1, sizeof(int)), (int*)calloc(1, sizeof(int)));
  SplayTree *tree = splay_tree_create(root);
  splay_tree_destroy(&tree);
  assert(!tree);
}
