#include "scapegoat.h"

ScapegoatNode *scapegoat_node_create(void *key, void *value) {
  // create scapegoat node
  ScapegoatNode *this = (ScapegoatNode*)calloc(1, sizeof(ScapegoatNode));
  if(this == NULL) {
    return this;
  }
  this->key = key;
  this->value = value;
  this->left = NULL;
  this->right = NULL;
  return this;
}
ULONG scapegoat_node_size(ScapegoatNode *this) {
  if(this == NULL) {
    return 0;
  }
  return 1 + scapegoat_node_size(this->left) + scapegoat_node_size(this->right);
}
void scapegoat_node_destroy(ScapegoatNode **this) {
  // destroy scapegoat node
  if(*this == NULL) {
    return;
  }
  free(*this);
  *this = NULL;
}
void scapegoat_node_linked_destroy(ScapegoatNode **this) {
  // destroy tree structure
  if(*this == NULL) {
    return;
  }
  ScapegoatNode *root = *this, *previous = NULL, *next;
  // todo use void instead of node
  while(root) {
    if(previous == root->parent) {
      // first previous = root->parent = NULL
      if(root->left) {
        next = root->left;
      } else if(root->right) {
        next = root->right;
      } else {
        next = root->parent;
        // start climbing up
      }
    } else if(previous == root->left) {
      // left branch
      if(root->right) {
        next = root->right;
      } else {
        next = root->parent;
        // start climbing up
      }
    } else {
      next = root->parent;
      // start climbing up
    }
    previous = root;
    if(next == root->parent) {
      scapegoat_node_destroy(&root);
    }
    root = next;
  }
  *this = NULL;
}
ScapegoatTree *scapegoat_tree_create(ScapegoatNode *root) {
  // scapegoat tree create
  ScapegoatTree *this = (ScapegoatTree*)calloc(1, sizeof(ScapegoatTree));
  if(this == NULL) {
    return this;
  }
  this->root = root;
  this->n = this->q = 1;
  return this;
}
DLONG scapegoat_tree_insert_with_depth(ScapegoatTree *this, ScapegoatNode *node, DLONG (*compare)(void *other, void *key)) {
  // scapegoat tree insert with depth
  ScapegoatNode *root = (ScapegoatNode*)this->root;
  UBYTE done = FALSE;
  DLONG res = FALSE, depth = 0;
  do {
    res = compare(node->key, root->key);
    if(res < 0) {
      if(!root->left) {
        node->parent = root;
        root->left = node;
        done = TRUE;
      } else {
        root = root->left;
      }
    } else if(res > 0) {
      if(!root->right) {
        node->parent = root;
        root->right = node;
        done = TRUE;
      } else {
        root = root->right;
      }
    } else {
      return -1;
    }
    this->n++;
    this->q++;
  } while(!done);
  return depth;
}
void scapegoat_tree_insert(ScapegoatTree *this, ScapegoatNode *node, DLONG (*compare)(void*, void*)) {
  // scapegoat tree insert
  int depth = scapegoat_tree_insert_with_depth(this, node, compare);
  ScapegoatNode *parent;
  if(depth > log32(this->q)) {
    // depth is larger, find scapegoat
    parent = node->parent;
    while(3 * scapegoat_node_size(parent) <= 2 * scapegoat_node_size(parent->parent)) {
      // while parent size less than parent parent
      parent = parent->parent;
    }
    scapegoat_tree_rebuild(this, parent->parent);
    // todo scapegoat_tree_rebuild
  }
}
ScapegoatNode *scapegoat_tree_find(ScapegoatTree *this, void *key, DLONG (*compare)(void*, void*)) {
  // find node with the key
  if(this == NULL) {
    return NULL;
  }
  ScapegoatNode *root = (ScapegoatNode*)this->root;
  DLONG res = FALSE;
  while(root) {
    res = compare(key, root->key);
    if(res == 0) {
      return root;
    } else if(res < 0) {
      root = root->left;
    } else {
      root = root->right;
    }
  }
  return root;
}
void scapegoat_tree_rebuild(ScapegoatTree *this, ScapegoatNode *parent) {
  // pack into array and build
  ULONG size = scapegoat_node_size(parent);
  ScapegoatNode *prnt = parent->parent, *root = (ScapegoatNode*)this->root, *child;
  ScapegoatNode **nodes = (ScapegoatNode**)array_create(size);
  scapegoat_tree_pack(nodes, prnt, 0);
  if(prnt == NULL) {
    this->root = scapegoat_tree_build(nodes, 0, size);
    root = (ScapegoatNode*)this->root;
    root->parent = NULL;
  } else if(root->right == parent) {
    root->right = scapegoat_tree_build(nodes, 0, size);
    child = (ScapegoatNode*)root->right;
    child->parent = root;
  } else {
    root->left = scapegoat_tree_build(nodes, 0, size);
    child = (ScapegoatNode*)root->left;
    child->parent = root;
  }
}
int scapegoat_tree_pack(ScapegoatNode **this, ScapegoatNode *parent, int i) {
  // pack into array
  // left tree parent right tree
  if(parent == NULL) {
    return i;
  }
  i = scapegoat_tree_pack(this, parent->left, i);
  this[i++] = parent;
  return scapegoat_tree_pack(this, parent->right, i);
}
ScapegoatNode *scapegoat_tree_build(ScapegoatNode **this, int i, int size) {
  // build from array
  if(!size) {
    return NULL;
  }
  int middle = size / 2;
  ScapegoatNode *child;
  this[i + middle]->left = scapegoat_tree_build(this, i, middle);
  // ham
  if(this[i + middle]->left != NULL) {
    child = (ScapegoatNode*)this[i + middle]->left;
    child->parent = this[i + middle];
  }
  this[i + middle]->right = scapegoat_tree_build(this, i + middle + 1, size - middle - 1);
  // ham
  if(this[i + middle]->right != NULL) {
    child = (ScapegoatNode*)this[i + middle]->right;
    child->parent = this[i + middle];
  }
  return this[i + middle];
  // ham
}
void scapegoat_tree_destroy(ScapegoatTree **this) {
  // scapegoat tree destroy
  if(*this == NULL) {
    return;
  }
  ScapegoatNode *root = (ScapegoatNode*)(*this)->root;
  // todo this differently
  scapegoat_node_linked_destroy(&root);
  // todo something recursive here
  free(*this);
  *this = NULL;
}
double log32(double q) {
  // log(3/2)(q)
  double log23 = 2.466; 
  // 1/log32
  return log(q) * log23;
}
