#include "scapegoat_test.h"

void scapegoat_node_create_test() {
  // scapegoat node create test
  ScapegoatNode *node = scapegoat_node_create((int*)calloc(1, sizeof(int)), (int*)calloc(1, sizeof(int)));
  assert(node);
  scapegoat_node_destroy(&node);
}
void scapegoat_node_destroy_test() {
  // scapegoat node destroy test
  ScapegoatNode *node = scapegoat_node_create((int*)calloc(1, sizeof(int)), (int*)calloc(1, sizeof(int)));
  scapegoat_node_destroy(&node);
  assert(!node);
}
void scapegoat_tree_create_test() {
  // scapegoat create tree test
  ScapegoatNode *root = scapegoat_node_create((int*)calloc(1, sizeof(int)), (int*)calloc(1, sizeof(int)));
  ScapegoatTree *tree = scapegoat_tree_create(root);
  assert(tree);
  scapegoat_tree_destroy(&tree);
}
void scapegoat_tree_insert_test() {
  // scapegoat insert tree test
  int *key, *value;
  DLONG capacity = 1024 * 1024, i = 0;
  time_t t;
  srand((unsigned)time(&t));
  key = (int*)calloc(1, sizeof(int));
  *key = rand() % capacity;
  value = (int*)calloc(1, sizeof(int));
  *value = rand() % capacity;
  ScapegoatNode *root = scapegoat_node_create(key, value), *node;
  ScapegoatTree *tree = scapegoat_tree_create(root);
  for(; i < capacity; i++) {
    key = (int*)calloc(1, sizeof(int));
    *key = rand() % capacity;
    value = (int*)calloc(1, sizeof(int));
    *value = rand() % capacity;
    node = scapegoat_node_create(key, value);
    scapegoat_tree_insert_with_depth(tree, node, int_compare);
  }
  assert(scapegoat_tree_find(tree, key, int_compare));
  scapegoat_tree_destroy(&tree);
}
void scapegoat_tree_destroy_test() {
  // scapegoat tree destroy test
  ScapegoatNode *root = scapegoat_node_create((int*)calloc(1, sizeof(int)), (int*)calloc(1, sizeof(int)));
  ScapegoatTree *tree = scapegoat_tree_create(root);
  scapegoat_tree_destroy(&tree);
  assert(!tree);
}
