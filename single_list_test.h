#ifndef SINGLE_LIST_TEST_H
#define SINGLE_LIST_TEST_H
#include <assert.h>
#include <time.h>
#include "single_list.h"

void single_node_create_test();
void single_node_destroy_test();
void list_create_test();
void list_insert_test();
void list_destroy_test();
#endif
