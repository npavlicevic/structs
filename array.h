#ifndef ARRAY_H
#define ARRAY_H
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#define TRUE 1
#define FALSE 0
typedef unsigned char UBYTE;
typedef unsigned long ULONG;
typedef long long DLONG;
DLONG int_compare(void *other, void *key);
void int_set(void **this, int position);
void int_print(void *this);
void array_init(void **this, int n, void (*set)(void **this, int position));
void array_swap(void **this, int from, int to);
void array_print(void **this, int n, void (*print_)(void *this));
void array_destroy(void ***this, int n);
void **array_create(ULONG size);
#endif
